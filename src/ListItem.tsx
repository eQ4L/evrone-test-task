import { memo, useEffect, useRef } from "react";

export default memo(({ index, item, onUpdate }) => {
  const renderCount = useRef(0);

  useEffect(() => {
    renderCount.current += 1;
  });

  const handleClick = () => {
    onUpdate(index);
  };

  return (
    <li>
      {item.label}: {item.value} (renders: {renderCount.current})
      <button onClick={handleClick}>Update</button>
    </li>
  );
});
