import { useCallback, useState } from "react";

import ListItem from "./ListItem";

export default function List({ itemsCount }) {
  const initialState = Array.from({ length: itemsCount }).map((_, id) => ({
    id,
    label: "Item #${id + 1}",
    value: Math.random()
  }));

  const [items, setItems] = useState(initialState);

  const handleUpdate = useCallback((index) => {
    setItems(prevItems => prevItems.map((item, idx) => {
      if (index === idx) return { ...item, value: Math.random() };

      return item;
    }));
  }, []);

  return (
    <>
      {/* <button>Delete first</button> */}
      <ul>
        {items.map((item, index) => (
          <ListItem
            key={index}
            index={index}
            item={item}
            onUpdate={handleUpdate}
          />
        ))}
      </ul>
    </>
  );
}
